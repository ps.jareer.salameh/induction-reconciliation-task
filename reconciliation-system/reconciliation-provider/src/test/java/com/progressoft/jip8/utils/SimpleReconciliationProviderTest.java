package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class SimpleReconciliationProviderTest {

    @Test
    public void givenNullFileValidator_whenConstruct_thenThrowNullPointerException() {
        ReportGenerator reportGenerator = new MockingReporter();
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new SimpleReconciliationProvider(null, reportGenerator, null));
        Assertions.assertEquals("null validator", nullPointerException.getMessage());
    }

    @Test
    public void givenNullReportGenerator_whenConstruct_thenThrowNullPointerException() {
        FileValidator fileValidator = new mockValidator();
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, ()
                -> new SimpleReconciliationProvider(fileValidator, null, null));
        Assertions.assertEquals("null generator", nullPointerException.getMessage());
    }

    @Test
    public void givenNullSource_getReport_thenThrowNullPointerException() {
        FileValidator fileValidator = new mockValidator();
        ReportGenerator reportGenerator = new MockingReporter();
        TransactionFileLoader mapper = new MockLoaderTransaction();
        SimpleReconciliationProvider simpleReconciliationProvider = new
                SimpleReconciliationProvider(fileValidator, reportGenerator, mapper);
        Path path = Paths.get("asdsadsa");
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, ()
                -> simpleReconciliationProvider.getReport(null, path));

        Assertions.assertEquals("null source", nullPointerException.getMessage());
    }

    @Test
    public void givenNullTarget_getReport_thenThrowNullPointerException() {
        FileValidator fileValidator = new mockValidator();
        ReportGenerator reportGenerator = new MockingReporter();
        TransactionFileLoader mapper = new MockLoaderTransaction();
        SimpleReconciliationProvider simpleReconciliationProvider =
                new SimpleReconciliationProvider(fileValidator, reportGenerator, mapper);
        Path path = Paths.get("sadsad");
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, ()
                -> simpleReconciliationProvider.getReport(path, null));

        Assertions.assertEquals("null target", nullPointerException.getMessage());
    }

    @Test
    public void givenNullFileMapper_whenConstruct_thenThrowNullPointerException() {
        FileValidator fileValidator = new mockValidator();
        ReportGenerator reportGenerator = new MockingReporter();
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new SimpleReconciliationProvider(fileValidator, reportGenerator, null));
        Assertions.assertEquals("null mapper", nullPointerException.getMessage());
    }
}
