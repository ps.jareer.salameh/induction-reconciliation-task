package com.progressoft.jip8.utils;

import java.nio.file.Path;
import java.util.List;

public class MockLoaderTransaction implements TransactionFileLoader {
    Transaction transaction = new Transaction( " " , " " , " " ," ");

    @Override
    public List<Transaction> getMappedFile(Path path) {
        List<Transaction> list = null;
        list.add(transaction);
        return list;
    }
}
