package com.progressoft.jip8.utils;

public class FileInfo {
    private String name;
    private String type;

    public FileInfo(String name, String target) {
        this.name = name;
        this.type = target;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}

