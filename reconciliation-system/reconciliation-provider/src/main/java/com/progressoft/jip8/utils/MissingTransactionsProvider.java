package com.progressoft.jip8.utils;

import java.util.List;

public interface MissingTransactionsProvider {
    public List<Transaction> getMissingTransaction(List<Transaction> sources, List<Transaction> target);
}
