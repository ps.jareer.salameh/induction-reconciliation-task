package com.progressoft.jip8.utils;

import java.util.List;

public interface ListGenerator {

    public List<Transaction> getMatchingRecords();

    public List<Transaction> getMisMatchingRecords();

    public List<Transaction> getMissingRecords();
}
