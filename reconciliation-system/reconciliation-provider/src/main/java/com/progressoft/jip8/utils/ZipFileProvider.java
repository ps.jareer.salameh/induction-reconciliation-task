package com.progressoft.jip8.utils;

import java.io.File;

public interface ZipFileProvider {
    byte[] prepareZipFile(File directory, String[] files, String path);
}
