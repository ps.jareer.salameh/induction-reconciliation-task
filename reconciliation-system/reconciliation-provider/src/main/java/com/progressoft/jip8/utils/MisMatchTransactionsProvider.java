package com.progressoft.jip8.utils;

import java.util.List;

public interface MisMatchTransactionsProvider {
    public List<Transaction> getMisMatchedTransaction(List<Transaction> sources,
                                                   List<Transaction> target);

}
