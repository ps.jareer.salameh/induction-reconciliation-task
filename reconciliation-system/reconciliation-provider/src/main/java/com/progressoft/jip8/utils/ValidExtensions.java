package com.progressoft.jip8.utils;

public enum ValidExtensions {
    CSV, JSON;
}
