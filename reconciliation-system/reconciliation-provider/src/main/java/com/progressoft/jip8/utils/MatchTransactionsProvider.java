package com.progressoft.jip8.utils;

import java.util.List;

public interface MatchTransactionsProvider {
    public List<Transaction> getMatchedTransaction(List<Transaction> sources,
                                                   List<Transaction> target);

}
