package com.progressoft.jip8.utils;

import java.nio.file.Path;
import java.util.List;

public interface ReportGenerator {

    void generateReport(List<Transaction> sourceTransactions ,List<Transaction> targetTransactions , String reportType) ;
}
