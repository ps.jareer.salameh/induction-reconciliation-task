package com.progressoft.jip8.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

public class SimpleReconciliationProvider implements ReconciliationProvider {

    private FileValidator fileValidator;
    private TransactionFileLoader transactionFileLoader;
    private ReportGenerator reportGenerator;

    SimpleReconciliationProvider(FileValidator fileValidator, ReportGenerator reportGenerator, TransactionFileLoader transactionFileLoader) {
        validate(fileValidator, reportGenerator, transactionFileLoader);
        this.transactionFileLoader = transactionFileLoader;
        this.fileValidator = fileValidator;
        this.reportGenerator = reportGenerator;
    }

    @Override
    public void getReport(Path source, Path target) throws IOException {
        validatePaths(source, target);
        fileValidator.validateFile(source);
        fileValidator.validateFile(target);
        List<Transaction> mappedSource = transactionFileLoader.getMappedFile(source);
        List<Transaction> mappedTarget = transactionFileLoader.getMappedFile(target);
        reportGenerator.generateReport(mappedSource, mappedTarget, "csv");
    }

    private void validatePaths(Path source, Path target) {
        if (Objects.isNull(source))
            throw new NullPointerException("null source");
        if (Objects.isNull(target))
            throw new NullPointerException("null target");
    }

    private void validate(FileValidator fileValidator, ReportGenerator reportGenerator, TransactionFileLoader transactionFileLoader) {
        if (Objects.isNull(fileValidator))
            throw new NullPointerException("null validator");
        if (Objects.isNull(reportGenerator))
            throw new NullPointerException("null generator");
        if (Objects.isNull(transactionFileLoader))
            throw new NullPointerException("null mapper");
    }
}
