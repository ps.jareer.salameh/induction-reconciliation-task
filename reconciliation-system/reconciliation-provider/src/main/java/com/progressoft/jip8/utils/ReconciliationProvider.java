package com.progressoft.jip8.utils;

import java.io.IOException;
import java.nio.file.Path;

public interface ReconciliationProvider {
    void getReport(Path source, Path target) throws IOException;
}
