package com.progressoft.jip8.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;

public interface FileValidator {
    void validateFile(Path path) throws IOException;
}
