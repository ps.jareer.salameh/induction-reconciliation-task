package com.progressoft.jip8.utils;

import java.io.File;
import java.util.List;

public interface FileInfoProvider {

    List<FileInfo> getFileInfo(File file);
}
