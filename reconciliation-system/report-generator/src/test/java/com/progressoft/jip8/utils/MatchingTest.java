package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MatchingTest {

    @Test
    public void givenNullTransactions_whenGetMatch_thenThrowNullPointerException() {
        MatchingListProvider matchingListProvider = new MatchingListProvider();
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, ()
                -> matchingListProvider.getMatchedTransaction(null, null));
        Assertions.assertEquals("null source or target", nullPointerException.getMessage());
    }

    @Test
    public void givenValidTransactions_whenGetMatch_thenWorkAsExpected() {
        MatchingListProvider matchingListProvider = new MatchingListProvider();
        List<Transaction> source = new ArrayList<>();
        List<Transaction> target = new ArrayList<>();
        source.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        source.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        source.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        target.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        target.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        target.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));

        List<Transaction> matchedTransaction = matchingListProvider.getMatchedTransaction(source, target);
        Assertions.assertNotNull(matchedTransaction);
        Object[] objects = matchedTransaction.toArray();
        int length = objects.length;
        Assertions.assertEquals(9 , length);


    }
}
