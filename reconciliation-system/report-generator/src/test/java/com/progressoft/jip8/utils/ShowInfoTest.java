package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ShowInfoTest {

    @Test
    public void givenFolder_whenGetInfo_thenWorkAsExpected() {

        FileInfoGenerator fileInfoGenerator = new FileInfoGenerator();
        List<FileInfo> fileInfo = fileInfoGenerator.getFileInfo(new File("src/main/java/com/progressoft/jip8/utils"));
        Assertions.assertNotNull(fileInfo);
    }

}
