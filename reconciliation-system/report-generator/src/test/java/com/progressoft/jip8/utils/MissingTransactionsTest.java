package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MissingTransactionsTest {
    @Test
    public void givenNullTransactions_whenGetMatch_thenThrowNullPointerException() {
        MissingListProvider missing = new MissingListProvider();
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, ()
                -> missing.getMissingTransaction(null, null));
        Assertions.assertEquals("null source or target", nullPointerException.getMessage());
    }

    @Test
    public void givenValidTransactions_whenGetMatch_thenWorkAsExpected() {
        MissingListProvider missing = new MissingListProvider();
        List<Transaction> source = new ArrayList<>();
        List<Transaction> target = new ArrayList<>();
        source.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        source.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        source.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        target.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        target.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));
        target.add(new Transaction("20/01/2020", "TR-47884222201", "140.00", "USD"));

        List<Transaction> missmatched = missing.getMissingTransaction(source, target);
        Assertions.assertNotNull(missmatched);
        Object[] objects = missmatched.toArray();
        int length = objects.length;
        Assertions.assertEquals(0, length);
    }
}
