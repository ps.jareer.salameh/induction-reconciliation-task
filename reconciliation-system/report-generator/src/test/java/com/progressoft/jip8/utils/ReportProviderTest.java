package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ReportProviderTest {


    @Test
    public void givenNull_whenGenerateReport_thenThrowNullPointerException() {
        ReportProvider reportProvider = new ReportProvider();
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, ()
                -> reportProvider.generateReport(null, null, "csv"));
        Assertions.assertEquals("null source transactions", nullPointerException.getMessage());
    }

    @Test
    public void givenNullTargetTransactions_whenGenerateReport_thenThrowNullPointerException() {
        Transaction transaction = new Transaction(" ", " ", " ", " ");
        List<Transaction> source = new ArrayList<>();
        source.add(transaction);
        ReportProvider reportProvider = new ReportProvider();
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, ()
                -> reportProvider.generateReport(source, null, "csv"));
        Assertions.assertEquals("null target transactions", nullPointerException.getMessage());
    }

    @Test
    public void givenValidTransactionsLists_whenGenerateReport_thenCreateFolderAndFiles() throws IOException {
        LoaderTransaction mapper = new LoaderTransaction();
        Path source = Paths.get("src" + File.separator + "test" + File.separator + "java" + File.separator + "com" + File.separator + "progressoft" + File.separator + "jip8" + File.separator + "utils" + File.separator + "online-banking-transactions.json");
        List<Transaction> mappedSourceFile = mapper.getMappedFile(source);
        Path target = Paths.get("src" + File.separator + "test" + File.separator + "java" + File.separator + "com" + File.separator + "progressoft" + File.separator + "jip8" + File.separator + "utils" + File.separator + "online-banking-transactions.json");
        List<Transaction> mappedTargetFile = mapper.getMappedFile(target);
        System.out.println(mappedSourceFile);
        System.out.println(mappedTargetFile);
        ReportProvider reportProvider = new ReportProvider();
        reportProvider.generateReport(mappedSourceFile, mappedTargetFile, "csv");
        Path matching = Paths.get("result"+ File.separator+"matching.csv");

            Assertions.assertFalse(isEmptyFile(matching));

    }

    private boolean isEmptyFile(Path path) throws IOException {
        return Files.size(path) == 0;
    }
}
