package com.progressoft.jip8.utils;

import java.io.*;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFileGenerator implements ZipFileProvider {
    @Override
    public byte[] prepareZipFile(File directory, String[] files, String path) {
        try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ZipOutputStream zos = new ZipOutputStream(baos);
            byte[] bytes = new byte[2048];
            for (String fileName : files) {
                addFilesToZipFolder(path, zos, bytes, fileName);
            }
            zos.flush();
            baos.flush();
            zos.close();
            baos.close();
            return baos.toByteArray();
        } catch (Exception e) {
            throw new IllegalStateException("error while downloading zip" + e.getMessage());
        }
    }

    public void addFilesToZipFolder(String path, ZipOutputStream zos, byte[] bytes, String fileName) throws IOException {
        FileInputStream fis = new FileInputStream(path + fileName);
        BufferedInputStream bis = new BufferedInputStream(fis);
        zos.putNextEntry(new ZipEntry(fileName));
        int bytesRead;
        while ((bytesRead = bis.read(bytes)) != -1) {
            zos.write(bytes, 0, bytesRead);
        }
        zos.closeEntry();
        bis.close();
        fis.close();
    }

}
