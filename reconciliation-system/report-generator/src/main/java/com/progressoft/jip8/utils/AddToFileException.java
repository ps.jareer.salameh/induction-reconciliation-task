package com.progressoft.jip8.utils;

public class AddToFileException extends RuntimeException {

    public AddToFileException(String s) {
        super(s);
    }

    public AddToFileException(String message, Exception cause) {
        super(message, cause);
    }
}
