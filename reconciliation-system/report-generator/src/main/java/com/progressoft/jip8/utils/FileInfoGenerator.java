package com.progressoft.jip8.utils;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileInfoGenerator implements FileInfoProvider {
    @Override
    public List<FileInfo> getFileInfo(File file) {

        validateFIle(file);
        File[] files = file.listFiles();
        List<FileInfo> filesInfo = new ArrayList<>();
        assert files != null;
        for (File value : files)
            filesInfo.add(new FileInfo(value.getName(), FilenameUtils.getExtension(value.getName())));
        return filesInfo;
    }

    public void validateFIle(File file) {
        if(Objects.isNull(file))
            throw  new NullPointerException("null file");
    }

}
