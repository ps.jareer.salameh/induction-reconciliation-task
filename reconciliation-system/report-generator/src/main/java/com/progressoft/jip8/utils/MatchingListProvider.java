package com.progressoft.jip8.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MatchingListProvider implements MatchTransactionsProvider {
    @Override
    public List<Transaction> getMatchedTransaction(List<Transaction> sources, List<Transaction> target) {
        validateTransactions(sources, target);
        return addMatchingTransactions(sources, target);
    }

    public List<Transaction> addMatchingTransactions(List<Transaction> sources, List<Transaction> target) {
        List<Transaction> matchedTransactions = new ArrayList<>();
        for (Transaction sourceTransaction : sources) {
            for (Transaction targetTransaction : target) {
                if (sourceTransaction.equals(targetTransaction))
                    matchedTransactions.add(sourceTransaction);
            }
        }
        return matchedTransactions;
    }

    public void validateTransactions(List<Transaction> sources, List<Transaction> target) {
        if (Objects.isNull(sources) || Objects.isNull(target))
            throw new NullPointerException("null source or target");
    }
}
