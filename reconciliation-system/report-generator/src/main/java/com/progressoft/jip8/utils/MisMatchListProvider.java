package com.progressoft.jip8.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MisMatchListProvider implements MisMatchTransactionsProvider {
    @Override
    public List<Transaction> getMisMatchedTransaction(List<Transaction> sources, List<Transaction> target) {
        validateTransactions(sources, target);
        return addMismatchingTransaction(sources, target);
    }

    public List<Transaction> addMismatchingTransaction(List<Transaction> sources, List<Transaction> target) {
        List<Transaction> missMatch = new ArrayList<>();

        for (Transaction sourceTransaction : sources) {
            for (Transaction targetTransaction : target) {
                if (sourceTransaction.getReference().equals(targetTransaction.getReference()) &&
                        isMissMatching(sourceTransaction, targetTransaction)) {
                    missMatch.add(sourceTransaction);
                    missMatch.add(targetTransaction);

                }
            }
        }
        return missMatch;
    }

    private void validateTransactions(List<Transaction> sources, List<Transaction> target) {
        if (Objects.isNull(sources) || Objects.isNull(target))
            throw new NullPointerException("null source or target");
    }

    private boolean isMissMatching(Transaction sourceTransaction, Transaction targetTransaction) {
        return !sourceTransaction.getAmount().equals(targetTransaction.getAmount()) ||
                !sourceTransaction.getMyDate().equals(targetTransaction.getMyDate()) ||
                !sourceTransaction.getCurrencyCode().equals(targetTransaction.getCurrencyCode());
    }
}
