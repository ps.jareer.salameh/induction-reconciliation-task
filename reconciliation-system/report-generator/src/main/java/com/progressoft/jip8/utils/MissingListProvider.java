package com.progressoft.jip8.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MissingListProvider implements MissingTransactionsProvider {
    @Override
    public List<Transaction> getMissingTransaction(List<Transaction> sources, List<Transaction> target) {

        validateTransactions(sources, target);
        List<Transaction> matchedInSource = new ArrayList<>();
        List<Transaction> matchedInTarget = new ArrayList<>();
        for (Transaction sourceTransaction : sources) {
            for (Transaction targetTransaction : target) {
                if (!hasSameId(sourceTransaction, targetTransaction))
                    continue;
                matchedInSource.add(sourceTransaction);
                matchedInTarget.add(targetTransaction);
                break;
            }
        }
        sources.removeAll(matchedInSource);
        target.removeAll(matchedInTarget);
        ArrayList<Transaction> missingTransactions = new ArrayList<>();
        missingTransactions.addAll(sources);
        missingTransactions.addAll(target);
        return missingTransactions;
    }

    private void validateTransactions(List<Transaction> sources, List<Transaction> target) {
        if (Objects.isNull(sources) || Objects.isNull(target))
            throw new NullPointerException("null source or target");
    }

    private boolean hasSameId(Transaction sourceTransaction, Transaction targetTransaction) {
        return sourceTransaction.getReference().equals(targetTransaction.getReference());
    }

}
