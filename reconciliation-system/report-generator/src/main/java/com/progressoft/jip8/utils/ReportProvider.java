package com.progressoft.jip8.utils;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.*;

public class ReportProvider implements ReportGenerator {

    @Override
    public void generateReport(List<Transaction> sourceTransactions, List<Transaction> targetTransactions, String reportType) {
        validateNullTransactions(sourceTransactions, targetTransactions);
        List<Transaction> matchedInSource = new ArrayList<>();
        List<Transaction> matchedInTarget = new ArrayList<>();

        for (Transaction sourceTransaction : sourceTransactions) {
            for (Transaction targetTransaction : targetTransactions) {
                if (!hasSameId(sourceTransaction, targetTransaction))
                    continue;
                handleSameTransactionId(sourceTransaction, targetTransaction, reportType);
                matchedInSource.add(sourceTransaction);
                matchedInTarget.add(targetTransaction);
                break;
            }
        }
        writeToMissingCsvFile(sourceTransactions,
                targetTransactions, matchedInSource, matchedInTarget, reportType);
    }

    public void handleSameTransactionId(Transaction sourceTransaction, Transaction targetTransaction, String reportType) {

        if (sourceTransaction.equals(targetTransaction)) {
            if (reportType.equals("csv"))
                addToMatchingCsvFile(sourceTransaction);
            else if (reportType.equals("json"))
                addToMatchingJsonFile(sourceTransaction);
            return;
        }
        if (reportType.equals("csv"))
            addToMismatchingCsvFile(sourceTransaction, targetTransaction);
        else if (reportType.equals("json"))
            addToMismatchingJsonFile(sourceTransaction, targetTransaction);


    }

    private void addToMismatchingJsonFile(Transaction sourceTransaction, Transaction targetTransaction) {

        Gson gson = new Gson();
        try (FileWriter file = new FileWriter("result/MisMatching.json", true)) {

            file.write(String.valueOf(gson.toJsonTree(sourceTransaction)));
            file.write(String.valueOf(gson.toJsonTree(targetTransaction)));

        } catch (IOException e) {
            throw new AddToFileException("error while adding miss matching json file" + e);
        }
    }

    private void addToMatchingJsonFile(Transaction sourceTransaction) {
        Gson gson = new Gson();
        try (FileWriter file = new FileWriter("result/Matching.json", true)) {

            file.write(String.valueOf(gson.toJsonTree(sourceTransaction)));
        } catch (IOException e) {
            throw new AddToFileException("error while adding matching json file" + e);
        }

    }


    private void writeToMissingCsvFile(List<Transaction> sourceTransactions, List<Transaction> targetTransactions, List<Transaction> matchedInSource, List<Transaction> matchedInTarget, String reportType) {
        sourceTransactions.removeAll(matchedInSource);
        targetTransactions.removeAll(matchedInTarget);
        if (reportType.equals("csv")) {
            addMissingTransactions(sourceTransactions, "SOURCE ");
            addMissingTransactions(targetTransactions, "TARGET");
        } else if (reportType.equals("json")) {
            addMissingJSONTransactions(sourceTransactions, "SOURCE ");
            addMissingJSONTransactions(targetTransactions, "TARGET");
        }

    }

    private void addMissingJSONTransactions(List<Transaction> transactions, String target) {
        Gson gson = new Gson();
        try (FileWriter file = new FileWriter("result/missing.json", true)) {

            for (Transaction transaction : transactions) {
                file.write(String.valueOf(gson.toJsonTree(transaction)));
            }
        } catch (IOException e) {
            throw new AddToFileException("error while adding missing json file" + e);
        }
    }

    private void addMissingTransactions(List<Transaction> sourceTransactions, String name) {
        for (Transaction sourceTransaction : sourceTransactions) {
            try (FileWriter writer = new FileWriter("result/Missing.csv", true)) {
                writer.write((name + sourceTransaction.getReference() + " , " +
                        sourceTransaction.getAmount() + " , " +
                        sourceTransaction.getCurrencyCode() + " , "
                        + sourceTransaction.getMyDate() + "\n"));
            } catch (IOException e) {
                throw new AddToFileException("error while add missing csv file" + e);
            }
        }
    }

    private void addToMismatchingCsvFile(Transaction sourceTransaction, Transaction targetTransaction) {
        try (FileWriter writer = new FileWriter("result/missMatching.csv", true);) {

            writer.write("SOURCE " + sourceTransaction.getReference() + " , " +
                    sourceTransaction.getAmount() + " , " +
                    sourceTransaction.getCurrencyCode() + " , "
                    + sourceTransaction.getMyDate() + "\n");
            writer.write("TARGET " + targetTransaction.getReference() + " , " +
                    targetTransaction.getAmount() + " , " +
                    targetTransaction.getCurrencyCode() + " , "
                    + targetTransaction.getMyDate() + "\n");

        } catch (IOException e) {
            throw new AddToFileException("error while adding missMatching file" , e);
        }
    }

    private void addToMatchingCsvFile(Transaction sourceTransaction) {
        try (FileWriter writer = new FileWriter("result/matching.csv", true);) {
            writer.write(sourceTransaction.getReference() + " , " +
                    sourceTransaction.getAmount() + " , " +
                    sourceTransaction.getCurrencyCode() + " , "
                    + sourceTransaction.getMyDate() + "\n");
        } catch (IOException e) {
            throw new AddToFileException("error while adding matching file csv" , e);
        }
    }

    private boolean hasSameId(Transaction sourceTransaction, Transaction targetTransaction) {
        return sourceTransaction.getReference().equals(targetTransaction.getReference());
    }

    private void validateNullTransactions(List<Transaction> sourceTransactions, List<Transaction> targetTransactions) {
        if (Objects.isNull(sourceTransactions))
            throw new NullPointerException("null source transactions");
        if (Objects.isNull(targetTransactions))
            throw new NullPointerException("null target transactions");
    }

}
