package com.progressoft.jip8.utils;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class FragmentsInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        FileValidator validator = new Validator();
        MatchTransactionsProvider matchTransactionsProvider = new MatchingListProvider();
        ReportGenerator reportGenerator = new ReportProvider();
        TransactionFileLoader mapper = new LoaderTransaction();
        SourceServlet sourceServlet = new SourceServlet();
        MisMatchTransactionsProvider misMatchTransactionsProvider = new MisMatchListProvider();
        MissingTransactionsProvider missingTransactionsProvider = new MissingListProvider();
        ZipFileProvider zipFileProvider = new ZipFileGenerator();
        FileInfoProvider fileInfoProvider = new FileInfoGenerator();

        registerGetFilesInfoApi(servletContext , fileInfoProvider);
        registerGenerateResultApi(servletContext, validator, reportGenerator, mapper);
        registerSource(servletContext, sourceServlet);
        registerTarget(servletContext);
        registerShowInfoServlet(servletContext);
        registerComparePageServlet(servletContext);
        uploadDownloadServletRegistration(servletContext, zipFileProvider ,validator );
        registerGetMissMatchApi(servletContext, mapper, misMatchTransactionsProvider);
        registerGetMissingApi(servletContext, mapper, missingTransactionsProvider);
        registerGetMatchApi(servletContext, mapper, matchTransactionsProvider);
        registerCompareNewFIlesApi(servletContext);
    }

    public void uploadDownloadServletRegistration(ServletContext servletContext, ZipFileProvider zipFileProvider , FileValidator fileValidator) {
        ServletRegistration.Dynamic servletRegistration =
                servletContext.addServlet("UploadDownloadFileServlet",
                        new UploadDownloadFileServlet(zipFileProvider ,fileValidator));
        servletRegistration.addMapping("/UploadDownloadFileServlet");
    }

    public void registerComparePageServlet(ServletContext servletContext) {
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("comparePageServlet", new ComparePageServlet());
        servletRegistration.addMapping("/compare");
    }

    public void registerShowInfoServlet(ServletContext servletContext) {
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("showInfoServlet", new ShowInfo());
        servletRegistration.addMapping("/showInfo");
    }

    public void registerTarget(ServletContext servletContext) {
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("targetServlet", new TargetServlet());
        servletRegistration.addMapping("/target");
    }

    public void registerSource(ServletContext servletContext, SourceServlet sourceServlet) {
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("sourceServlet", sourceServlet);
        servletRegistration.addMapping("/source");
    }

    private void registerGetMissingApi(ServletContext servletContext, TransactionFileLoader mapper, MissingTransactionsProvider missingTransactionsProvider) {
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet
                ("missing", new GetMissingApi(mapper, missingTransactionsProvider));
        servletRegistration.addMapping("/getMissing");
    }

    private void registerGetMissMatchApi(ServletContext servletContext, TransactionFileLoader mapper, MisMatchTransactionsProvider misMatchTransactionsProvider) {
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet
                ("MissMatched", new GetMisMatchingApi(mapper, misMatchTransactionsProvider));
        servletRegistration.addMapping("/getMissMatch");
    }

    public void registerGetMatchApi(ServletContext servletContext, TransactionFileLoader mapper, MatchTransactionsProvider matchTransactionsProvider) {
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet
                ("matched", new GetMatchApi(mapper, matchTransactionsProvider));
        servletRegistration.addMapping("/getMatch");
    }

    public void registerCompareNewFIlesApi(ServletContext servletContext) {
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("compareNewFiles", new CompareNewFilesApi());
        servletRegistration.addMapping("/compareNewFiles");
    }


    public void registerGenerateResultApi(ServletContext servletContext, FileValidator validator, ReportGenerator reportGenerator, TransactionFileLoader mapper) {
        ServletRegistration.Dynamic servletRegistration1 =
                servletContext.addServlet("generateResult",
                        new GenerateResultApi(reportGenerator, validator, mapper));
        servletRegistration1.addMapping("/generateResultFiles");
    }

    public void registerGetFilesInfoApi(ServletContext servletContext, FileInfoProvider fileInfoProvider) {
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("getFiles", new GetFilesAPI(fileInfoProvider));
        servletRegistration.addMapping("/getFiles");
    }
}
