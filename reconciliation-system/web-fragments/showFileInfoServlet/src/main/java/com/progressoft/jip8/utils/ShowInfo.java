package com.progressoft.jip8.utils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ShowInfo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        checkIfAuthorized(req, resp);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/result.jsp");
        requestDispatcher.forward(req, resp);
    }

    private void checkIfAuthorized(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("UserName") == null) {
            resp.sendRedirect("/");
            return;
        }
        if (session.getAttribute("sourceUpload") == null) {
            resp.sendRedirect("/source");
            return;
        }
        if (session.getAttribute("targetUpload") == null) {
            resp.sendRedirect("/target");
            return;
        }
    }
}
