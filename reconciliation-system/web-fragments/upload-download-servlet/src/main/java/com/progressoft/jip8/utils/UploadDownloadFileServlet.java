package com.progressoft.jip8.utils;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;


public class UploadDownloadFileServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ServletFileUpload uploader = null;
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");
    private ZipFileProvider zipFileProvider;
    private FileValidator fileValidator;

    public UploadDownloadFileServlet(ZipFileProvider zipFileProvider, FileValidator fileValidator) {
        this.zipFileProvider = zipFileProvider;
        this.fileValidator = fileValidator;
    }

    @Override
    public void init() {
        DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        File filesDir = (File) getServletContext().getAttribute("FILES_DIR_FILE");
        fileFactory.setRepository(filesDir);
        this.uploader = new ServletFileUpload(fileFactory);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        checkIfAuthorized(request, response);
        File directory = new File("result");
        String[] files = directory.list();

        if (checkIfDirectoryIsReady(files)) {
            String path = directory.getPath() + UploadDownloadFileServlet.FILE_SEPARATOR;
            byte[] zipBytes = zipFileProvider.prepareZipFile(directory, files, path);
            ServletOutputStream sos = response.getOutputStream();
            response.setContentType("application/zip");
            sos.write(zipBytes);
            sos.flush();
        }
    }

    private boolean checkIfDirectoryIsReady(String[] files) {
        return files != null && files.length > 0;
    }

    private void checkIfAuthorized(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("UserName") == null) {
            response.sendRedirect("/");
            return;
        }
        if (session.getAttribute("sourceUpload") == null) {
            response.sendRedirect("/source");
            return;
        }
        if (session.getAttribute("targetUpload") == null)
            response.sendRedirect("/target");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        uploadSetup(request, response);
        redirectToTargetPage(request, response);
    }

    private void uploadSetup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!ServletFileUpload.isMultipartContent(request)) {
            throw new ServletException("Content type is not multipart/form-data");
        }
        try {
            writeFileToDirectory(request);
        } catch (Exception e) {
            // TODO so, what was the problem
            File uploadedFiles = new File("uploaded/tmpfiles");
            FileUtils.cleanDirectory(uploadedFiles);
            response.sendRedirect("/source");
            return;
        }
        File file = new File("uploaded/tmpfiles");
        int numberOfUploadedFiles = getNumberOfUploadedFiles(file);
        // TODO what if not uploaded
        if (areUploaded(numberOfUploadedFiles)) {
            redirectToShowInfoPage(request, response);
        }
    }

    private void writeFileToDirectory(HttpServletRequest request) throws Exception {
        List<FileItem> fileItemsList = uploader.parseRequest(request);
        for (FileItem fileItem : fileItemsList) {
            File file = new File(request.getServletContext().getAttribute("FILES_DIR") + File.separator + fileItem.getName());
            fileItem.write(file);
            fileValidator.validateFile(Paths.get(file.getAbsolutePath()));
        }
    }

    private void redirectToTargetPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().setAttribute("sourceUpload", "sourceUpload");
        response.sendRedirect("/target");
        return;
    }

    private void redirectToShowInfoPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // TODO put true/false
        request.getSession().setAttribute("targetUpload", "targetUpload");
        response.sendRedirect("/showInfo");
        return;
    }

    private boolean areUploaded(int numberOfUploadedFiles) {
        return numberOfUploadedFiles >= 2;
    }

    private int getNumberOfUploadedFiles(File file) {
        return Objects.requireNonNull(file.listFiles()).length;
    }

}
