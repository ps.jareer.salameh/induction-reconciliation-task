package com.progressoft.jip8.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GetFilesAPI extends HttpServlet {
    private Gson gson;
    FileInfoProvider fileInfoProvider;

    public GetFilesAPI(FileInfoProvider fileInfoProvider) {
        this.fileInfoProvider = fileInfoProvider;
    }

    @Override
    public void init() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        // TODO you should have a path for each user
        File folder = new File("uploaded" + File.separator + "tmpfiles");
        List<FileInfo> fileInfo = fileInfoProvider.getFileInfo(folder);
        resp.setContentType("application/json");
        gson.toJson(fileInfo, resp.getWriter());
    }
}
