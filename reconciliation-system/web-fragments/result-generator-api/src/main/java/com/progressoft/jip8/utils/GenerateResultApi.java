package com.progressoft.jip8.utils;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class GenerateResultApi extends HttpServlet {
    private ReportGenerator reportGenerator;
    private FileValidator validator;
    private TransactionFileLoader mapper;

    public GenerateResultApi(ReportGenerator reportGenerator, FileValidator validator, TransactionFileLoader mapper) {
        this.reportGenerator = reportGenerator;
        this.validator = validator;
        this.mapper = mapper;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String outputResultExtension = req.getParameter("extension");
        File folder = new File("uploaded" + File.separator + "tmpfiles");
        File[] files = folder.listFiles();
        assert files != null;
        // TODO the user should know the source of missing record
        Path source = Paths.get(files[0].getPath());
        Path target = Paths.get(files[1].getPath());
        generateReport(outputResultExtension, source, target);
        resp.sendRedirect("/compare");
    }

    // TODO enhance: you could move this method along with the dependencies to a separate layer (CompareHandler)
    private void generateReport(String extension, Path source, Path target) throws IOException {
        validator.validateFile(source);
        validator.validateFile(target);
        List<Transaction> sourceFile = mapper.getMappedFile(source);
        List<Transaction> targetFile = mapper.getMappedFile(target);
        reportGenerator.generateReport(sourceFile, targetFile, extension);
    }
}
