package com.progressoft.jip8.utils;

import org.apache.commons.io.FileUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

public class CompareNewFilesApi extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        deleteFiles();
        removeSessions(req, resp);
    }

    private void removeSessions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        session.removeAttribute("targetUpload");
        session.removeAttribute("sourceUpload");
        resp.sendRedirect("source");
    }

    private void deleteFiles() throws IOException {
        File uploadedFiles = new File("uploaded/tmpfiles");
        File resultFiles = new File("result");
        FileUtils.cleanDirectory(resultFiles);
        FileUtils.cleanDirectory(uploadedFiles);
    }
}
