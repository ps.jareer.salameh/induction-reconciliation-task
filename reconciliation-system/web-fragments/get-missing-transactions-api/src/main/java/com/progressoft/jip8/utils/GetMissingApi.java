package com.progressoft.jip8.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import java.util.List;

public class GetMissingApi extends HttpServlet {

    private TransactionFileLoader mapper;
    private MissingTransactionsProvider missingTransactionsProvider;
    private Gson gson;

    public GetMissingApi(TransactionFileLoader mapper, MissingTransactionsProvider missingTransactionsProvider) {
        this.mapper = mapper;
        this.missingTransactionsProvider = missingTransactionsProvider;
    }

    @Override
    public void init() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        File folder = new File("uploaded/tmpfiles");
        File[] files = folder.listFiles();
        assert files != null;
        List<Transaction> sourceTransactions = mapper.getMappedFile(Paths.get(files[0].getPath()));
        List<Transaction> targetTransactions = mapper.getMappedFile(Paths.get(files[1].getPath()));
        List<Transaction> missingTransactions = missingTransactionsProvider.getMissingTransaction(sourceTransactions, targetTransactions);

        resp.setContentType("application/json");
        gson.toJson(missingTransactions, resp.getWriter());
    }
}
