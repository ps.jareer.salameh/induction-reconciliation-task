package com.progressoft.jip8.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;


public class GetMatchApi extends HttpServlet {
    private TransactionFileLoader mapper;
    private MatchTransactionsProvider matchTransactionsProvider;
    private Gson gson;

    // TODO you could have only one servlet to handle three types of the transactions,
    public GetMatchApi(TransactionFileLoader mapper, MatchTransactionsProvider matchTransactionsProvider) {
        this.mapper = mapper;
        this.matchTransactionsProvider = matchTransactionsProvider;
    }
    @Override
    public void init() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        File folder = new File("uploaded" + File.separator + "tmpfiles");
        File[] files = folder.listFiles();
        assert files != null;
        List<Transaction> sourceTransactions = mapper.getMappedFile(Paths.get(files[0].getPath()));
        List<Transaction> targetTransactions = mapper.getMappedFile(Paths.get(files[1].getPath()));
        List<Transaction> matchedTransactions = matchTransactionsProvider.getMatchedTransaction(sourceTransactions, targetTransactions);
        resp.setContentType("application/json");
        gson.toJson(matchedTransactions, resp.getWriter());
    }
}
