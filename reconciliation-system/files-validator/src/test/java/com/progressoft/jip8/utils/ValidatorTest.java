package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ValidatorTest {
    static Validator validator;
    public static Path empty;

    @BeforeAll
    public static void setup() {
        validator = new Validator();
    }

    @BeforeAll
    public static void copySampleFile() throws IOException {
        try (InputStream is = Validator.class.getResourceAsStream(File.separator + "empty.csv")) {
            empty = Files.createTempFile("data1", ".csv");
            try (OutputStream os = Files.newOutputStream(empty)) {
                int length;
                byte[] buffer = new byte[1024 * 8];
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
                os.flush();
            }
        }
    }

    @Test
    public void givenNullPath_whenValidate_thenThrowNullPointerException() {
        Path path = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, ()
                -> validator.validateFile(path));
        Assertions.assertEquals("null path", exception.getMessage());
    }

    @Test
    public void givenNonExistPath_whenConstructing_thenThrowFileNotFoundException() {
        Path path = Paths.get("jareer salameh");
        FileNotFoundException fileNotFoundException =
                Assertions.assertThrows(FileNotFoundException.class, ()
                        -> validator.validateFile(path));
        Assertions.assertEquals(" file not found", fileNotFoundException.getMessage());
    }

    @Test
    public void givenEmptyFile_whenConstructing_thenThrowIllegalStateException() {
        Path path = Paths.get(String.valueOf(empty));
        IllegalStateException illegalStateException =
                Assertions.assertThrows(IllegalStateException.class, ()
                        -> validator.validateFile(path));
        Assertions.assertEquals("empty  file", illegalStateException.getMessage());
    }


    @Test
    public void givenUnsupportedFileExtension_whenConstructing_thenThrowIllegalStateException() {
        Path path = Paths.get("src" + File.separator + "test" + File.separator + "resources" + File.separator + "text.txt");
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows
                (IllegalArgumentException.class, () ->
                        validator.validateFile(path));
        Assertions.assertEquals("the file extension not supported", illegalArgumentException.getMessage());
    }

    @Test
    public void givenInvalidCsvFileFormat_whenConstruct_thenThrowIllegalStateException() throws IOException {
        Path path = Paths.get("src" + File.separator + "test" + File.separator + "resources" + File.separator + "full.csv");
        IllegalStateException illegalStateException =
                Assertions.assertThrows(IllegalStateException.class, ()
                        -> validator.validateFile(path));
        Assertions.assertEquals("invalid header", illegalStateException.getMessage());

    }

    @Test
    public void givenCsvWithRepeatedId_whenConstruct_thenThrowIllegalStateException() throws IOException {
        Path path = Paths.get("src/test/resources/repeated.csv");

        IllegalStateException illegalStateException =
                Assertions.assertThrows(IllegalStateException.class, () ->
                        validator.validateFile(path));
        Assertions.assertEquals("the Ids in Csv source is not unique", illegalStateException.getMessage());

    }

    @Test
    public void givenCsvFileWithInvalidHeader_whenValidate_thenThrowIllegalStateException() {
        Path path = Paths.get("src/test/resources/invalidheader.csv");
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class,
                () -> validator.validateFile(path));
        Assertions.assertEquals("invalid header", illegalStateException.getMessage());

    }

    @Test
    public void givenJsonWithInvalidKeys_whenValidate_thenThrowIllegalStateException() {
        Path path = Paths.get("src/test/resources/invalid.json");
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, () ->
                validator.validateFile(path));
        Assertions.assertEquals("invalid keys format", illegalStateException.getMessage());
    }

    @Test
    public void givenJsonFileFileWithInvalidNumberOfKeys_whenValidate_thenThrowIllegalStateException() {
        Path path = Paths.get("src/test/resources/invalidnumber.json");
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, () ->
                validator.validateFile(path));
        Assertions.assertEquals("invalid keys format", illegalStateException.getMessage());
    }
}
