package com.progressoft.jip8.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Set;

import static com.google.common.io.Files.getFileExtension;

public class Validator implements FileValidator {

    @Override
    public void validateFile(Path path) throws IOException {
        validateAnyFile(path);
        String extension = getExtension(path);
        if (isCsvFIle(extension)) {
            validateCsvFiles(path);
            return;
        }
        if (isJsonFIle(extension))
            validateJsonFiles(path);
    }

    private void validateJsonFiles(Path path) {
        try {
            checkKeys(path);
        } catch (ParseException | IOException e) {
            throw new IllegalStateException("problem with json file");
        }
    }

    private void checkKeys(Path path) throws IOException, ParseException {
        JSONParser jsonParser;
        jsonParser = new JSONParser();
        try (FileReader reader = new FileReader(String.valueOf(path))) {
            JSONArray transactionList = (JSONArray) jsonParser.parse(reader);
            for (Object transaction : transactionList) {
                Set set = ((JSONObject) transaction).keySet();
                if (!isValidKeys(set))
                    throw new IllegalStateException("invalid keys format");
            }
        }
    }

    private boolean isValidKeys(Set set) {
        return set.contains("amount") && set.contains("date")
                && set.contains("reference") && set.contains("purpose")
                && set.contains("currencyCode");
    }

    private void validateCsvFiles(Path path) throws IOException {
        checkCsvFormat(path);
    }

    private void validateAnyFile(Path path) throws IOException {
        checkNull(path);
        checkIfExist(path);
        checkIfEmpty(path);
        checkExtensions(path);
    }

    private boolean isaValidCsvHeader(String[] part) {
        return part[0].equalsIgnoreCase("trans unique id") &&
                part[1].equalsIgnoreCase("trans description") &&
                part[2].equalsIgnoreCase("amount") &&
                part[3].equalsIgnoreCase("currecny") &&
                part[4].equalsIgnoreCase("purpose") &&
                part[5].equalsIgnoreCase("value date") &&
                part[6].equalsIgnoreCase("trans type");
    }

    private boolean isCsvFIle(String extension) {
        return extension.equals(ValidExtensions.CSV.toString().toLowerCase());
    }

    private boolean isJsonFIle(String extension) {
        return extension.equals(ValidExtensions.JSON.toString().toLowerCase());
    }

    private boolean isValidExtension(String extension, String s) {
        return extension.equals(s);
    }

    private void checkCsvFormat(Path path) throws IOException {
        String lines;
        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            lines = bufferedReader.readLine();
            checkId(bufferedReader);
        }
        String[] parts = lines.split(",");
        if (!isaValidCsvHeader(parts))
            throw new IllegalStateException("invalid header");
        if (parts.length != 7)
            throw new IllegalStateException("number of columns in csv should be 7");
    }

    private void checkId(BufferedReader bufferedReader) throws IOException {
        ArrayList<String> ids = new ArrayList<>();
        skipHeader(bufferedReader);
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] myLine = line.split(",");
            ids.add(myLine[0]);
        }
        if (isRepeated(ids))
            throw new IllegalStateException("the Ids in Csv source is not unique");
    }

    private void checkExtensions(Path path) {
        if (!isValidExtension(path))
            throw new IllegalArgumentException("the file extension not supported");
    }

    private void checkIfEmpty(Path path) throws IOException {
        if (isEmptyFile(path))
            throw new IllegalStateException("empty  file");
    }

    private boolean isEmptyFile(Path path) throws IOException {
        return Files.size(path) == 0;
    }

    private void checkIfExist(Path path) throws FileNotFoundException {
        if (isNotExist(path))
            throw new FileNotFoundException(" file not found");
    }

    private boolean isNotExist(Path path) {
        return Files.notExists(path);
    }

    private void checkNull(Path path) {
        if (isNull(path))
            throw new NullPointerException("null path");
    }

    private boolean isNull(Path path) {
        return Objects.isNull(path);
    }

    private boolean isValidExtension(Path path) {
        boolean validExtension = false;
        String sourceFileExtension = getExtension(path);
        ValidExtensions[] extensions = ValidExtensions.values();
        String s = sourceFileExtension.toUpperCase();
        for (ValidExtensions extension : extensions)
            if (isValidExtension(extension.toString(), s)) {
                validExtension = true;
                break;
            }
        return validExtension;
    }

    private String getExtension(Path path) {
        return getFileExtension(String.valueOf(path));
    }

    private void skipHeader(BufferedReader bf) throws IOException {
        bf.readLine();
    }

    public boolean isRepeated(ArrayList<String> input) {
        for (int partOne = 0; partOne < input.size(); partOne++) {
            for (int partTow = 0; partTow < input.size(); partTow++) {
                if (input.get(partOne).equals(input.get(partTow)) && partOne != partTow)
                    return true;
            }
        }
        return false;
    }
}