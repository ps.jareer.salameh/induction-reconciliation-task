package com.progressoft.jip8.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class app {
    public static void main(String[] args) throws IOException {
        ReportGenerator reportGenerator = new ReportProvider();

        System.out.println("Please enter the first file path  : ");
        Scanner scanner = new Scanner(System.in);
        String firstPath = scanner.nextLine();
        System.out.println("Please enter the second file  :");
        String secondPath = scanner.nextLine();
        Path source = Paths.get(firstPath);
        Path target = Paths.get(secondPath);
        FileValidator validator = new Validator();
        ///home/user/Desktop/sample-files_input-files_bank-transactions (7).csv
        ///home/user/Desktop/online-banking-transactions.json
        TransactionFileLoader mapper = new LoaderTransaction();
        ReconciliationProvider reconciliationProvider = new SimpleReconciliationProvider(validator, reportGenerator, mapper);
        reconciliationProvider.getReport(source, target);
        System.out.println(" you can find the the files in /home/user/reconciliation-results");
        // /home/user/gitlab/reconciliation-system/report-generator/src/test/resources/valid.csv
        // /home/user/gitlab/reconciliation-system/report-generator/src/test/resources/valid.csv
    }
}
