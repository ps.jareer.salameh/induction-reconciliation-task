package com.progressoft.jip8.utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.when;


public class LoginServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenDispatcher_whenDoGet_thenRunAsExpected() throws IOException, ServletException {
        when(request.getRequestDispatcher("/index.jsp")).thenReturn(new RequestDispatcherMock());
        LoginServlet signInServlet = new LoginServlet();
        Assertions.assertDoesNotThrow(() -> {
            signInServlet.doGet(request, response);
        });
    }

    private class RequestDispatcherMock implements RequestDispatcher {
        int callCount = 0;

        @Override
        public void forward(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
            Assertions.assertEquals(callCount, 0);
            callCount++;
            Assertions.assertEquals(servletRequest, request);
            Assertions.assertEquals(servletResponse, response);
        }

        @Override
        public void include(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
            Assertions.fail();
        }
    }
}
