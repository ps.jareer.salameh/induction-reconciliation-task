<html>
<head>
  <title>Source Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container mt-3">
  <h2>Reconciliation system Information page</h2>
<div class="container center_div">

<form action="/generateResultFiles" method="post" >
  <div class="form-group">
  <div class="card" style="width: 18rem;">
    <div class="card-body">
      <h5 class="Source">Source File Information</h5>
     <span>Source name :</span> <span id="sourceName"></span>
     <div></div>
     <span>Source Type :</span>
<span id="sourceType"></span>

    </div>
      <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="Source">Target File Information</h5>
         <span>Target name :</span> <span id="targetName"></span>
         <div></div>
         <span>Target Type :</span>
    <span id="targetType"></span>

        </div>
  </div>



<div>Select results Extensions</div>
    <select  name="extension" class="custom-select">
        <option value="csv">csv</option>
        <option value ="json">json</option>

    </select>
    <div class="mt-3">
        <input type="submit" value="compare" class="btn btn-primary">
    </div>
    </div>
</form>
<form action="compareNewFiles" method="get" >
    <div class="mt-3">
        <input type="submit" value="cancel   " class="btn btn-primary">
    </div>

</form>

<div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $.ajax({
            url: "http://localhost:8080/getFiles",
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                    $("#sourceName").html(result[0].name);
                    $("#sourceType").html(result[0].type);
                    $("#targetName").html(result[1].name);
                    $("#targetType").html(result[1].type);
            }
        });
    });
</script>

</body>
</html>