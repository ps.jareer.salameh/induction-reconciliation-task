<html>
<body>
<head>
    <style>
table {
    width: 50%;
}
th {
    background: #f1f1f1;
    font-weight: bold;
    padding: 6px;
}
td {
    background: #f9f9f9;
    padding: 6px;
}
</style>
  <title>show and download result</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
</head>
<form action="UploadDownloadFileServlet" method="get">

  <h2>Show & download result page</h2>

    <div class="mt-3">
        <input type="submit" value="download result    " class="btn btn-primary">
    </div>

</form>
<form action="compareNewFiles" method="get">
    <div class="mt-3">
        <input type="submit" value="compare new files" class="btn btn-primary">
    </div>
</form>


<button type="button" id = "match" class="btn btn-primary">show matching</button>
<button type="button" id = "missmatch"class="btn btn-primary">show mismatching</button>
<button type="button" id = "missing"class="btn btn-primary">show missing</button>


<table id="myTable">
    <tr>
        <th>Id</th>
        <th>Code</th>
        <th>Amount</th>
        <th>Date</th>

    </tr>
</table>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
  $("#match").on("click", function () {
            $.ajax({
    url: 'http://localhost:8080/getMatch',
    type: "GET",
    contentType: "application/json",
    success: function(data) {
    $('#myTable').html("");
        for (var i=0; i<data.length; i++) {
            var row = $('<tr><td>' + data[i].reference+ '</td><td>' + data[i].currencyCode + '</td><td>' + data[i].amount + '</td><td>' + data[i].date + '</td></tr>');
            $('#myTable').append(row);
        }
    },
    error: function(jqXHR, textStatus, errorThrown){
        alert('Error: ' + textStatus + ' - ' + errorThrown);
    }
});
        });

  $("#missmatch").on("click", function () {
            $.ajax({
    url: 'http://localhost:8080/getMissMatch',
    type: "GET",
    contentType: "application/json",
    success: function(data) {
   $('#myTable').html("");
        for (var i=0; i<data.length; i++) {
            var row = $('<tr><td>' + data[i].reference+ '</td><td>' + data[i].currencyCode + '</td><td>' + data[i].amount + '</td><td>' + data[i].date + '</td></tr>');
            $('#myTable').append(row);
        }
    },
    error: function(jqXHR, textStatus, errorThrown){
        alert('Error: ' + textStatus + ' - ' + errorThrown);
    }
});
        });
  $("#missing").on("click", function () {
            $.ajax({
    url: 'http://localhost:8080/getMissing',
    type: "GET",
    contentType: "application/json",
    success: function(data) {
    $('#myTable').html("");
        for (var i=0; i<data.length; i++) {
            var row = $('<tr><td>' + data[i].reference+ '</td><td>' + data[i].currencyCode + '</td><td>' + data[i].amount + '</td><td>' + data[i].date + '</td></tr>');
            $('#myTable').append(row);
        }
    },
    error: function(jqXHR, textStatus, errorThrown){
        alert('Error: ' + textStatus + ' - ' + errorThrown);
    }
});
        });

</script>



</body>
</html>