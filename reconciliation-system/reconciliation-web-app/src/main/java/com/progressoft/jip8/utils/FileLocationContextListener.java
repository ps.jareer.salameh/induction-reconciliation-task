package com.progressoft.jip8.utils;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;

@WebListener
public class FileLocationContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        tempDirectorySetUp(servletContextEvent);
    }

    public void tempDirectorySetUp(ServletContextEvent servletContextEvent) {
        ServletContext ctx = servletContextEvent.getServletContext();
        String relativePath = ctx.getInitParameter("tempfile.dir");
        File file = new File("uploaded" + File.separator + relativePath);
        if (fileNotFound(file))
            file.mkdirs();
        ctx.setAttribute("FILES_DIR_FILE", file);
        ctx.setAttribute("FILES_DIR", "uploaded" + File.separator + relativePath);
    }

    public boolean fileNotFound(File file) {
        return !file.exists();
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }

}