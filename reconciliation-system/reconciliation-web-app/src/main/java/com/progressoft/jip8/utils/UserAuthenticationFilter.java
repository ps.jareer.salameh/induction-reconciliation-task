package com.progressoft.jip8.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.*;

public class UserAuthenticationFilter implements Filter {

    private List<Users> authorizedUsers = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) {
        addMockUsers(authorizedUsers);
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        String userName = getUserName(req);
        String password = getPassword(req);

        for (Users authorizedUser : authorizedUsers)
            if (isAuthorizedUser(userName, password, authorizedUser)) {
                chain.doFilter(req, resp);
                return;// TODO you should return
            }

        RequestDispatcher rd = req.getRequestDispatcher("/index.jsp");
        rd.include(req, resp);
    }

    public boolean isAuthorizedUser(String userName, String password, Users authorizedUser) {
        return authorizedUser.getUsername().equals(userName) && authorizedUser.getPassword().equals(password);
    }

    private void addMockUsers(List<Users> authorizedUsers) {
        authorizedUsers.add(new Users("admin", "admin"));
        authorizedUsers.add(new Users("jareer", "salameh"));
        authorizedUsers.add(new Users("mohammad", "shawkat"));
    }

    private String getPassword(ServletRequest req) {
        return req.getParameter("password");
    }

    private String getUserName(ServletRequest req) {
        return req.getParameter("name");
    }

    private static class Users {
        private String username;
        private String password;

        public Users(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }
    }

    @Override
    public void destroy() {
    }
}