package com.progressoft.jip8.utils;

public class MappingException extends RuntimeException {
    public MappingException(String s) {
        super(s);
    }
}
