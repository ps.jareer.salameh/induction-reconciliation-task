package com.progressoft.jip8.utils;

import org.apache.commons.lang3.time.DateUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.Currency;
import java.util.Date;
import java.util.Objects;

public class Transaction {
    private String date;
    private String reference;
    private String amount;
    private String currencyCode;

    public Transaction(String date, String reference, String amount, String currencyCode) {

        this.date = date;
        this.reference = reference;
        this.amount = amount;
        this.currencyCode = currencyCode;
    }

    public String getMyDate() {
        Date myDate = parseDate(date);
        return myDate.toString();
    }

    public String getDate() {
        return date;
    }

    private int numDigits(int num) {
        int count = 0;
        while (num != 0) {
            num /= 10;
            ++count;
        }
        return count;
    }

    public String getReference() {
        return reference;
    }

    public String getAmount() {
        BigDecimal bigDecimal = new BigDecimal(amount);
        int scale = Currency.getInstance(getCurrencyCode()).getDefaultFractionDigits();
        return bigDecimal.setScale(scale, RoundingMode.HALF_UP).toString();
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return
                Objects.equals(getMyDate(), that.getMyDate()) &&
                        Objects.equals(reference, that.reference) &&
                        Objects.equals(getAmount(), that.getAmount()) &&
                        Objects.equals(currencyCode, that.currencyCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference, amount, currencyCode, date);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "date='" + date + '\'' +
                ", reference='" + reference + '\'' +
                ", amount='" + amount + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                '}';
    }

    private Date parseDate(String inputDate) {
        inputDate = inputDate.replace('/', '-');
        Date outputDate;
        String[] possibleDateFormats = {"yyyy-MM-dd", "dd-MM-yyyy"};

        try {
            String[] split = inputDate.split("-");
            int numberOfDigitsInFirstPart = Integer.parseInt(split[0]);
            if (numDigits(numberOfDigitsInFirstPart) > 2)
                outputDate = DateUtils.parseDate(inputDate, possibleDateFormats[0]);
            else
                outputDate = DateUtils.parseDate(inputDate, possibleDateFormats[1]);
        } catch (ParseException e) {
            throw new IllegalStateException("Problem when parse date");
        }
        return outputDate;
    }
}