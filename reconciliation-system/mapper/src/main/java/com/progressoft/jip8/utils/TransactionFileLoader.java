package com.progressoft.jip8.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface TransactionFileLoader {
    List<Transaction> getMappedFile(Path path) throws IOException;
}
