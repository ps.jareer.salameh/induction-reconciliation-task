package com.progressoft.jip8.utils;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class LoaderTransaction implements TransactionFileLoader {
    @Override
    public List<Transaction> getMappedFile(Path path) throws IOException {
        List<Transaction> transactions;
        checkPath(path);
        if (isCsvFile(path)) {
            transactions = mapCsv(path);
            return transactions;
        }
        if (isJsonFile(path)) {
            transactions = mapJson(path);
            return transactions;
        }
        throw new IllegalStateException("unsupported file");
    }

    private void checkPath(Path path) {
        if (Objects.isNull(path))
            throw new NullPointerException("null path");
    }

    private List<Transaction> mapCsv(Path path) throws IOException {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        ArrayList<Transaction> csvTransactions = new ArrayList<>();
        try {

            br = new BufferedReader(new FileReader(String.valueOf(path)));
            line = br.readLine();
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] parts = line.split(cvsSplitBy);
                csvTransactions.add(new Transaction(parts[5] , parts[0] , parts[2] , parts[3]));

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return csvTransactions;
    }

    private List<Transaction> mapJson(Path source) {
        try {
            JsonReader reader;
            reader = new JsonReader(new FileReader(String.valueOf(source)));
            Gson gson = new Gson();
            Transaction[] founderArray = gson.fromJson(reader, Transaction[].class);
            List<Transaction> transactions = new ArrayList(Arrays.asList(founderArray));
            return transactions;

        } catch (FileNotFoundException e) {
            throw new MappingException("error while map json files");
        }
    }

    private boolean isCsvFile(Path path) {
        return getExtension(path).toLowerCase().equals("csv");
    }

    private boolean isJsonFile(Path path) {
        return getExtension(path).toLowerCase().equals("json");
    }

    private String getExtension(Path source) {
        return com.google.common.io.Files.getFileExtension(String.valueOf(source));
    }
}
