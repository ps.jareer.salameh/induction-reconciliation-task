package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MapperTest {
    @Test
    public void givenNullPath_whenGetMappedFile_thenThrowNullPointerException() {
        Path path = null;
        LoaderTransaction mapper = new LoaderTransaction();
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> mapper.getMappedFile(path));
        Assertions.assertEquals("null path", nullPointerException.getMessage());
    }

    @Test
    public void givenUnsupportedFile_whenGetMappedFile_IllegalStateException() {
        Path path = Paths.get("jareer.exe");
        LoaderTransaction mapper = new LoaderTransaction();
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class,
                () -> mapper.getMappedFile(path));
        Assertions.assertEquals("unsupported file", illegalStateException.getMessage());
    }

    @Test
    public void givenCsvFile_whenGetMappedFile_ThenWorkAsExpected() throws IOException {
        Path path = Paths.get("src" + File.separator + "test"+ File.separator + "resources"+ File.separator + "valid.csv");
        LoaderTransaction mapper = new LoaderTransaction();
        List<Transaction> mappedFile = mapper.getMappedFile(path);
        Assertions.assertNotNull(mappedFile);
        for (Transaction transaction : mappedFile) {
            Assertions.assertNotNull(transaction.getAmount());
            Assertions.assertNotNull(transaction.getMyDate());
            Assertions.assertNotNull(transaction.getCurrencyCode());
            Assertions.assertNotNull(transaction.getReference());
        }
    }

    @Test
    public void givenJsonFile_whenGetMappedFile_ThenWorkAsExpected() throws IOException {
        Path path = Paths.get("src"+ File.separator + "test"+ File.separator + "resources"+ File.separator + "valid.json");
        LoaderTransaction mapper = new LoaderTransaction();
        List<Transaction> mappedFile = mapper.getMappedFile(path);
        Assertions.assertNotNull(mappedFile);
        for (Transaction transaction : mappedFile) {
            Assertions.assertNotNull(transaction.getAmount());
            Assertions.assertNotNull(transaction.getMyDate());
            Assertions.assertNotNull(transaction.getCurrencyCode());
            Assertions.assertNotNull(transaction.getReference());
        }
    }
//    FileValidator fileValidator = null;
//    ReportGenerator reportGenerator = new MockingReporter();
//
//    NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> new SimpleReconciliationProvider(fileValidator, reportGenerator));
//
//        Assertions.assertEquals("null validator", nullPointerException.getMessage());


}
